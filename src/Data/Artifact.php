<?php
/**
 * This file is part of the BluBubble PharBuild package.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade BluBubble PharBuild
 * to newer versions in the future.
 *
 * @copyright Copyright (c) 2017 BluBubble, Ltd.(https://blububble.xyz/)
 * @license   GNU General Public License ("GNU GPL") v. 3.0
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BluBubble\PharBuild\Data;

/**
 * Artifact model.
 *
 * @author Raimonds Vizulis <info@blububble.xyz>
 */
class Artifact
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $entry;

    /**
     * Artifact constructor.
     *
     * @param string $name
     * @param string $entry
     */
    public function __construct(string $name, string $entry)
    {
        $this->name = $name;
        $this->entry = $entry;
    }

    /**
     * Get artifact name.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Get artifact entry.
     *
     * @return string
     */
    public function getEntry(): string
    {
        return $this->entry;
    }
}
