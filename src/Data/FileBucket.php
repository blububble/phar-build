<?php
/**
 * This file is part of the BluBubble PharBuild package.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade BluBubble PharBuild
 * to newer versions in the future.
 *
 * @copyright Copyright (c) 2017 BluBubble, Ltd.(https://blububble.xyz/)
 * @license   GNU General Public License ("GNU GPL") v. 3.0
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BluBubble\PharBuild\Data;

/**
 * File bucket.
 *
 * @author Raimonds Vizulis <info@blububble.xyz>
 */
class FileBucket
{
    /**
     * @var File[]
     */
    protected $files;

    /**
     * FileBucket constructor.
     *
     * @param File[] $files
     */
    public function __construct(array $files)
    {
        $this->files = $files;
    }

    /**
     * Get all files.
     *
     * @return File[]
     */
    public function getFiles(): array
    {
        return array_filter($this->files, function(File $file) {
            return !$file->isIgnored();
        });
    }

    /**
     * Get ignored files.
     *
     * @return File[]
     */
    public function getIgnoredFiles(): array
    {
        return array_filter($this->files, function(File $file) {
            return $file->isIgnored();
        });
    }
}
