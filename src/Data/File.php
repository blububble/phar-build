<?php
/**
 * This file is part of the BluBubble PharBuild package.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade BluBubble PharBuild
 * to newer versions in the future.
 *
 * @copyright Copyright (c) 2017 BluBubble, Ltd.(https://blububble.xyz/)
 * @license   GNU General Public License ("GNU GPL") v. 3.0
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BluBubble\PharBuild\Data;

/**
 * Class File.
 *
 * @author Raimonds Vizulis <info@blububble.xyz>
 */
class File
{
    /**
     * @var string
     */
    protected $path;

    /**
     * @var bool
     */
    protected $ignored;

    /**
     * File constructor.
     *
     * @param string $path
     * @param bool $ignored
     */
    public function __construct(string $path, bool $ignored = false)
    {
        $this->path = $path;
        $this->ignored = $ignored;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return bool
     */
    public function isIgnored(): bool
    {
        return $this->ignored;
    }
}
