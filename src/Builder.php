<?php
/**
 * This file is part of the BluBubble PharBuild package.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade BluBubble PharBuild
 * to newer versions in the future.
 *
 * @copyright Copyright (c) 2017 BluBubble, Ltd.(https://blububble.xyz/)
 * @license   GNU General Public License ("GNU GPL") v. 3.0
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BluBubble\PharBuild;

use BluBubble\PharBuild\Data\File;
use FilesystemIterator;
use Phar;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

/**
 * Build PHAR archive.
 *
 * @author Raimonds Vizulis <info@blububble.xyz>
 */
class Builder
{
    /**
     * Process PHAR archive.
     *
     * @param array $configuration
     *
     * @return void
     */
    public function execute(array $configuration): void
    {
        $phar = $this->createPhar($configuration['artifact']->getName() . '.phar');
        $ignoredFiles = [];

        foreach ($configuration['files']->getIgnoredFiles() as $ignoredFile) {
            /** @var File $ignoredFile */
            $ignoredFiles[] = $ignoredFile->getPath();
        }

        foreach ($configuration['files']->getFiles() as $file) {
            /** @var File $file */
            if (!is_dir($file->getPath())) {
                $this->addFile($file->getPath(), $phar);
                continue;
            }

            $this->processPath($file->getPath(), $phar, $ignoredFiles);
        }

        $phar->setStub($phar->createDefaultStub($configuration['artifact']->getEntry()));
    }

    /**
     * Read dir content.
     *
     * @param string $path
     * @param Phar $phar
     * @param array $ignoredFiles
     *
     * @return void
     */
    protected function processPath(string $path, Phar &$phar, array $ignoredFiles): void
    {
        $dir = new RecursiveDirectoryIterator($path);
        $iterator = new RecursiveIteratorIterator($dir);

        foreach ($iterator as $name => $object) {
            if ($this->isFileIgnored($name, $ignoredFiles)) {
                continue;
            }

            if (is_dir($name) || substr($name, -1) === '.') {
                continue;
            }

            $this->addFile($name, $phar);
        }
    }

    /**
     * Add file to PHAR archive.
     *
     * @param string $path
     * @param Phar $phar
     *
     * @return void
     */
    protected function addFile(string $path, Phar &$phar): void
    {
        $pharFile = str_replace(getcwd(), '', $path);
        $phar[$pharFile] = file_get_contents($path);
    }

    /**
     * Check if file is in ignored list.
     *
     * @param string $path
     * @param array $ignoredFiles
     *
     * @return bool
     */
    protected function isFileIgnored(string $path, array $ignoredFiles)
    {
        $files = array_filter($ignoredFiles, function(string $file) use ($path) {
            return strpos($file, $path) === 0;
        });

        return (bool) count($files);
    }

    /**
     * Init PHAR archive.
     *
     * @param string $name
     *
     * @return Phar
     */
    private function createPhar(string $name): Phar
    {
        return new Phar(
            getcwd() . DIRECTORY_SEPARATOR . $name,
            FilesystemIterator::CURRENT_AS_FILEINFO | FilesystemIterator::KEY_AS_FILENAME,
            $name
        );
    }
}
