<?php
/**
 * This file is part of the BluBubble PharBuild package.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade BluBubble PharBuild
 * to newer versions in the future.
 *
 * @copyright Copyright (c) 2017 BluBubble, Ltd.(https://blububble.xyz/)
 * @license   GNU General Public License ("GNU GPL") v. 3.0
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BluBubble\PharBuild\Console;

use BluBubble\PharBuild\Builder;
use BluBubble\PharBuild\Parser;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Phar builder command.
 *
 * @author Raimonds Vizulis <info@blububble.xyz>
 */
class Build extends Command
{
    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * @var Parser
     */
    protected $parser;
    /**
     * @var Builder
     */
    private $builder;

    /**
     * Builder constructor.
     *
     * @param Filesystem $filesystem
     * @param Parser $parser
     * @param Builder $builder
     */
    public function __construct(
        Filesystem $filesystem,
        Parser $parser,
        Builder $builder
    ) {
        parent::__construct();

        $this->filesystem = $filesystem;
        $this->parser = $parser;
        $this->builder = $builder;
    }

    /**
     * Configure command.
     *
     * @return void
     */
    protected function configure(): void
    {
        $this
            ->setName('build')
            ->setDescription('Build phar archive')
            ->addOption('config', 'c', InputOption::VALUE_REQUIRED, 'Configuration file');
    }

    /**
     * Execute build command.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     * @throws RuntimeException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $path = $input->getOption('config');

        if (!$path) {
            throw new RuntimeException('No configuration specified!');
        }

        if (!$this->filesystem->isAbsolutePath($path)) {
            $path = getcwd() . DIRECTORY_SEPARATOR .  $path;
        }

        if (!file_exists($path)) {
            throw new RuntimeException('Configuration file ' . $path . ' does not exist!');
        }

        $this->builder->execute($this->parser->parse($path));
    }
}
