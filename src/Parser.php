<?php
/**
 * This file is part of the BluBubble PharBuild package.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade BluBubble PharBuild
 * to newer versions in the future.
 *
 * @copyright Copyright (c) 2017 BluBubble, Ltd.(https://blububble.xyz/)
 * @license   GNU General Public License ("GNU GPL") v. 3.0
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BluBubble\PharBuild;

use BluBubble\PharBuild\Data\Artifact;
use BluBubble\PharBuild\Data\File;
use BluBubble\PharBuild\Data\FileBucket;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Parse build configuration file.
 *
 * @author Raimonds Vizulis <info@blububble.xyz>
 */
class Parser
{
    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * @var array
     */
    protected $config = [];

    /**
     * Parser constructor.
     *
     * @param Filesystem $filesystem
     */
    public function __construct(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    /**
     * Parse build configuration file.
     *
     * @param string $path
     *
     * @return array
     */
    public function parse(string $path): array
    {
        $xml = new \SimpleXMLElement(file_get_contents($path));

        foreach ($xml->children() as $child) {
            /** @var \SimpleXMLElement $child */
            switch ($child->getName()) {
                case 'artifact':
                    $this->processArtifact($child);
                    break;

                case 'files':
                    $this->processFiles($child);
                    break;
            }
        }

        return $this->config;
    }

    /**
     * Parse artifact element.
     *
     * @param \SimpleXMLElement $element
     *
     * @return void
     */
    protected function processArtifact(\SimpleXMLElement $element): void
    {
        $this->config['artifact'] = new Artifact(
            $this->getAttribute($element, 'name'),
            $this->getAttribute($element, 'entry')
        );
    }

    /**
     * Parse file element.
     *
     * @param \SimpleXMLElement $element
     *
     * @return void
     */
    protected function processFiles(\SimpleXMLElement $element): void
    {
        $files = [];

        foreach ($element->children() as $child) {
            /** @var \SimpleXMLElement $child */
            foreach ($child->attributes() as $key => $value) {
                if ($key !== 'path') {
                    continue;
                }

                $value = $this->filesystem->isAbsolutePath($value) ? $value : getcwd() . DIRECTORY_SEPARATOR . $value;

                if ($child->getName() === 'include') {
                    $files[] = new File($value);
                    continue;
                }

                $files[] = new File($value, true);
            }
        }

        $this->config['files'] = new FileBucket($files);
    }

    /**
     * Get XML element attribute.
     *
     * @param \SimpleXMLElement $element
     * @param string $attribute
     *
     * @return string|null
     */
    private function getAttribute(\SimpleXMLElement $element, string $attribute)
    {
        return (string) $element->$attribute[0] ?? null;
    }
}